export const persona = [
  { value: null, text: 'Selecione um paciente' },
  { value: 1, text: 'Ricky and Morty', img: require('../assets/img/ricky-and-morty.jpg'), description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec at faucibus erat.' },
  { value: 2, text: 'Gandalf the White', img: require('../assets/img/gandalf.jpg'), description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec at faucibus erat.' },
  { value: 3, text: 'Tony Stark', img: require('../assets/img/tony.jpg'), description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec at faucibus erat.' }
]