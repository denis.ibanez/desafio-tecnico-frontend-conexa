import Vue from 'vue'
import { localize, ValidationProvider, extend, ValidationObserver } from 'vee-validate'
import { required } from 'vee-validate/dist/rules'

extend('required', required)

localize({
  en: {
    messages: {
      required: 'Esse campo é obrigatório.'
    }
  }
})

Vue.component('ValidationProvider', ValidationProvider)
Vue.component('ValidationObserver', ValidationObserver)