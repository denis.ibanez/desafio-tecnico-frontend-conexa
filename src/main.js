// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import Vuex from 'vuex'
import store from './store/index'
import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'
import './plugins/vee-validate'

Vue.use(Vuex)

Vue.config.productionTip = false

Vue.use(BootstrapVue)
// Optionally install the BootstrapVue icon components plugin
Vue.use(IconsPlugin)

router.beforeEach((to, from, next) => {
  const authVerificate = to.matched.some(record => record.meta.requiresAuth)
  const visitorsVeficate = to.matched.some(record => record.meta.requiresVisitor)  
  if (authVerificate) {
    // this route requires auth, check if logged in
    // if not, redirect to login page.
    if (!store.getters.loggedIn) {
      next({
        path: '',
        query: { redirect: to.fullPath }
      })
    } else {
      next()
    }
  } else if (visitorsVeficate) {
    if (store.getters.loggedIn) {
      next({
        path: '/add-query',
        query: { redirect: to.fullPath }
      })
    } else {
      next()
    }
  } else {
    next() // make sure to always call next()!
  }
})

/* eslint-disable no-new */
new Vue({
  el: '#app',
  store,
  components: { App },
  router,
  template: '<App/>'
})

