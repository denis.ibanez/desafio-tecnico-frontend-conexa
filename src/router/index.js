import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/pages/Home'
import ListInput from '@/pages/ListInput'
import AddQuery from '@/pages/AddQuery'
import NotFound from '@/pages/NotFound'

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home,
      meta: {
        requiresVisitor: true
      }
    },
    {
      path: '/list-input',
      name: 'listInput',
      component: ListInput,
      meta: {
        requiresAuth: true
      }
    },
    {
      path: '/add-query',
      name: 'addQuery',
      component: AddQuery,
      meta: {
        requiresAuth: true
      }
    },
    { 
      path: '*',
      name: 'not-found',
      component: NotFound
    }  
  ]
})
