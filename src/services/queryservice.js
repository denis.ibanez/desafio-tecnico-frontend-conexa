import axios from 'axios'
import store from '../store/index'

const config = {
  headers: { Authorization: `Bearer ${store.getters.credentials.token}` }
}

export default class QueryService {
  getAllQuery() {
    const request = axios.get(`${process.env.BASE_PATH}api/consultas`, config)
      .then(response => response)
      .catch(e => {
        throw e
      }).finally(() => {

      })
    return request
  }

  getQueryById(id) {
    const request = axios.get(`${process.env.BASE_PATH}api/consulta/${id}`, config)
      .then(response => response)
      .catch(e => {
        throw e
      }).finally(() => {

      })
    return request
  }

  addQuery(param) {
    const request = axios.post(`${process.env.BASE_PATH}api/consulta`, param, config)
    .then(response => response)
    .catch(e => {
      throw e
    }).finally(() => {
    })
    return request
  }
}