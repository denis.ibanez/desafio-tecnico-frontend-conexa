import axios from 'axios'

export default class AuthService {
  getAuthService(param) {
    const request = axios.post(`${process.env.BASE_PATH}/api/login`, param)
      .then(response => response)
      .catch(e => {
        throw e
      }).finally(() => {

      })
    return request
  }
}