export default {
  state: {
    feedback: {
      error: { 
        msg: false,
        text: 'Ocorreu um erro, por favor tente novamente.' 
      },
      success: {
        msg: false,
        text: 'Sua consulta foi adicionada com sucesso!'
      }
    }
  },

  mutations: {
    controlFeedbackVisibilite(state, param) {
      state.feedback[param.type].msg = param.msg
    },

    controlFeedbackMessage(state, param) {
      state.feedback[param.type].text = param.text
    },
  },

  actions: {
    controlFeedbackVisibilite(context, param) {
      context.commit('controlFeedbackVisibilite', param)
    },

    controlFeedbackMessage(context, param) {
      context.commit('controlFeedbackMessage', param)
    }
  },

  getters: {
    feedback: state => {
      return state.feedback
    }
  }
}