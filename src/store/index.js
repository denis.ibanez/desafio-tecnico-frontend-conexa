import Vue from 'vue'
import Vuex from 'vuex'

import home from './home.js'
import feedback from './feedback-request.js'

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    home,
    feedback
  }
})