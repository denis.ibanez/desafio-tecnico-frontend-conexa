import AuthService from '@/services/authservice'
const authService = new AuthService()

export default {
  state: {
    credentials: {
      token: localStorage.getItem('access_token') || null,
      name: localStorage.getItem('name') || null,
      email: null
    }
  },

  mutations: {
    updateToken(state, param) {
      state.credentials.token = param.token
      state.credentials.name = param.nome
      state.credentials.email = param.email
    },
  },

  actions: {
    getToken(context, param) {
      return new Promise((resolve, reject) => {
        const credentials = {
          email: param.username,
          senha: param.password
        }
        authService.getAuthService(credentials).then((response) => {
          const access_token = response.data.data
          localStorage.setItem('access_token', access_token.token)
          localStorage.setItem('name', access_token.nome)
          context.commit('updateToken', access_token)
          resolve(response)
        }).catch((error) => {
          reject(error)
          console.log(error)
        })
      })
    },

    logoffAction(context) {
      const credencials = {
        token: null,
        name: null,
        email: null
      }
      localStorage.removeItem('access_token')
      localStorage.removeItem('name')
      context.commit('updateToken', credencials)
    }
  },

  getters: {
    credentials: state => {
      return state.credentials
    },

    loggedIn: state => {
      return state.credentials.token != null
    }
  }
}